# addok-places

Plugins to import places into addok

## Install 
Using pip

```
pip install git+ssh://git@git.vizitools.com/back-end/addok-places.git
```

You can also setup the specials plugins for france
```
pip install addok-fr addok-france
```

Make sure that you have the following configuration in `/etc/addok/addok.conf`
```
QUERY_PROCESSORS_PYPATHS = [
    "addok.helpers.text.check_query_length",
    "addok_france.extract_address",
    "addok_france.clean_query",
    "addok_france.remove_leading_zeros",
]
SEARCH_RESULT_PROCESSORS_PYPATHS = [
    "addok.helpers.results.match_housenumber",
    "addok_france.make_labels",
    "addok.helpers.results.score_by_importance",
    "addok.helpers.results.score_by_autocomplete_distance",
    "addok.helpers.results.score_by_ngram_distance",
    "addok.helpers.results.score_by_geo_distance",
]
PROCESSORS_PYPATHS = [
    "addok.helpers.text.tokenize",
    "addok.helpers.text.normalize",
    "addok_france.glue_ordinal",
    "addok_france.fold_ordinal",
    "addok_france.flag_housenumber",
    "addok.helpers.text.synonymize",
    "addok_fr.phonemicize",
]
```


## Import data

### BANO
First, we'll import the bano data.

In a development environment, I suggest you to download only a small part of the data (or your laptop may die), you can for example download a department.

Let's download it

```
# full bano
wget http://bano.openstreetmap.fr/data/full.sjson.gz

# department
wget http://bano.openstreetmap.fr/BAN_odbl/BAN_odbl_69-json.bz2
```

Unzip it and import it using the `addok batch` command 
```
gunzip full.sjson.gz
addok batch full.sjson
```

The import may take a while.

### Places
This plugin add a new command `places` enabling addok to import places from an osm file.

Using [pyosmium](https://github.com/osmcode/pyosmium), the plugin will parse every node of type [**place**](https://wiki.openstreetmap.org/wiki/Key:place) or [**shop**](https://wiki.openstreetmap.org/wiki/Key:shop) and will insert them into addok. 

The problem is, in a node, we don't have the administrative informations (city, region etc...), so we'll have to query an other service named [Pelias Point-in-Polygon Service](https://github.com/pelias/pip-service) to have them.

[Pip](https://github.com/pelias/pip-service) is a webservice that you can call with a coordinate as parameter and will return a json with all the administrative informations: the city, the region, the country, the continent... It also return for each administrative area, a bounding boxof the area that we'll also store in addok for the cities, country , etc.. so the map could zoom in/out to display the whole city/country.

Just copy/paste the following lines or go read the [pip doc](https://github.com/pelias/pip-service) if you want to know more.

```bash
git clone git@github.com:pelias/pip-service.git
cd pip-service
npm install
npm run download # download whosonfirst data
npm run start # start the server
```
[Try it](http://localhost:3102/5.276754/45.598851)


Let's download a file from geofrabik.
```
wget https://download.geofabrik.de/europe/france-latest.osm.pbf
``` 
Import it using the `addok places` command.

```
addok places --file france-latest.osm.pbf
```
