"""
Search for shops in an osm file and list their names.
"""
from addok.config import config
import osmium
import requests
from addok_places.osmium.admin_lookup import lookup


class PlacesHandler(osmium.SimpleHandler):

    results = []

    def process_node(self, tags, location):
        """
        Filter the shop and place nodes then process an admin lookup using the addok http api
        """
        if (tags.get("shop") or tags.get("place")) and location:
            if "name" in tags:
                name = tags["name"].encode("utf-8").decode("utf-8")
                if name is None:
                    pass
                address = tags.get("address", "").encode("utf-8").decode("utf-8")
                print('tag =>', tags.get('place'))
                need_bbox = tags.get('place') is not None
                geocode = lookup(location, need_bbox)
                try:
                    print(name)
                    # todo get location and merge geocode with doc
                    if geocode:
                        doc = {
                            "name": name,
                            "lat": location.lat,
                            "lon": location.lon,
                            "city": geocode["city"],
                            "citycode": geocode["citycode"] if 'citycode' in geocode else '',
                            "postcode": geocode["postcode"] if 'postcode' in geocode else '',
                            "context": geocode["context"] if 'context' in geocode else '',
                        }
                        print(doc)
                        self.results.append(doc)
                except:
                    pass

    def node(self, n):
        """
        For each nodes found, launch the process_node function 
        """
        self.process_node(n.tags, n.location)
