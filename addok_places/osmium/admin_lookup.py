from addok_places import config
import osmium
import requests


def lookup_addok(location, needs_bbox=False):
    """
    Query the pip service and return administrative information 
    about the location passed in parameters
    """
    response = requests.get(
        config.ADDOK_URL, params={"lat": location.lat, "lon": location.lon}
    )
    json = response.json()
    res = {}
    try:
        properties = json["features"][0]['properties']
        res["city"] = properties["city"]
        res["postcode"] = properties["postcode"]
    except:
        return None
    return res


def lookup_pip(location, needs_bbox=False):
    """
    Query the pip service and return administrative information 
    about the location passed in parameters
    """
    url = f"{config.PIP_URL}{location.lon}/{location.lat}"
    print(url)
    response = requests.get(url)
    json = response.json()
    res = {}
    try:
        res["city"] = json["locality"][0]["name"]
        res["bbox"] = json["locality"][0]["bounding_box"]
    except:
        try:
            res["city"] = json["localadmin"][0]["name"]
            res["bbox"] = json["localadmin"][0]["bounding_box"]
        except:
            return None
    return res


def lookup(location, needs_bbox=False):
    """
    Query both pip and addok service and return administrative information 
    about the location passed in parameters
    """
    res = None
    if needs_bbox:
        res = lookup_pip(location, needs_bbox)
    if not res:
        res = lookup_addok(location, needs_bbox)
    if not res and not needs_bbox:
        res = lookup_pip(location, needs_bbox)
    print(res)
    return res 
